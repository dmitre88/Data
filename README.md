# Data 

Clean data files from different topics. All the data files come from open-sources and may have been clean, so they are ready to use for data analysis.

* Gasoline prices and distance 052118.csv: file used for my blog post at https://dmitre88.github.io/2018/06/06/Does-competition-correlate-to-lower-gasoline-prices-and-price-dispersion.html
* Homicides rate per municipality 2010-2017.csv: estimated from INEGI data.
* INEGI homicides at national level 1990-2017.csv: estimated from INEGI data. Made for my blog post at https://dmitre88.github.io/2018/08/20/Patterns-of-homicide-rates-in-Mexico.html
* map_ENT.rds: a SpatialPolygonsDataFrame object from Mexico local states.
* map_MUN.rds: a simplified SpatialPolygonsDataFrame object from Mexico municipalities using the R-gsimplify function from rgeos.
* Homicides rates 2010-2017.gif: gif-file made for my blog post at https://dmitre88.github.io/2018/08/20/Patterns-of-homicide-rates-in-Mexico.html
* Mexican states names all caps.csv: Mexican state names and codes used for merging data. Retrieved from http://www.inegi.org.mx/geo/contenidos/geoestadistica/CatalogoClaves.aspx
* Municipal codes INEGI.csv: Mexican municipalities names and codes used for merging data. Retrieved from http://www.inegi.org.mx/geo/contenidos/geoestadistica/CatalogoClaves.aspx
